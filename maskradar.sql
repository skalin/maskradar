#
# Encoding: Unicode (UTF-8)
#


DROP TABLE IF EXISTS `Alarm`;
DROP TABLE IF EXISTS `Availtime`;
DROP TABLE IF EXISTS `Category`;
DROP TABLE IF EXISTS `Comment`;
DROP TABLE IF EXISTS `Currency`;
DROP TABLE IF EXISTS `Item`;
DROP TABLE IF EXISTS `Shop`;
DROP TABLE IF EXISTS `SubCategory`;
DROP TABLE IF EXISTS `User`;


CREATE TABLE `Alarm` (
  `id` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `user_id` int(20) DEFAULT NULL,
  `prize` float DEFAULT '0',
  `category_id` int(20) DEFAULT NULL,
  `subcategory_id` int(20) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `altitude` double DEFAULT NULL,
  `alarmOnline` bit(1) DEFAULT NULL,
  `numberOfItems` int(20) DEFAULT NULL,
  `currency_id` int(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `Availtime` (
  `id` int(11) DEFAULT NULL,
  `openingTime` time DEFAULT NULL,
  `closingTime` time DEFAULT NULL,
  `monday` bit(1) DEFAULT NULL,
  `tuesday` bit(1) DEFAULT NULL,
  `wednesday` bit(1) DEFAULT NULL,
  `thursday` bit(1) DEFAULT NULL,
  `friday` bit(1) DEFAULT NULL,
  `saturday` bit(1) DEFAULT NULL,
  `sunday` bit(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `Category` (
  `id` int(11) DEFAULT NULL,
  `categoryTranslationKey` varchar(128) DEFAULT NULL,
  `defaultPic` varchar(255) DEFAULT NULL,
  `infoTranslationKey` varchar(128) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `Comment` (
  `id` int(11) DEFAULT NULL,
  `commentText` text,
  `date` datetime DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `user_id` int(128) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `Currency` (
  `id` int(11) DEFAULT NULL,
  `currency` varchar(128) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `Item` (
  `id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `creationDate` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  `numberAvail` int(11) DEFAULT NULL,
  `isVerified` bit(1) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `pricePerUnit` float DEFAULT NULL,
  `orderURL` varchar(255) DEFAULT NULL,
  `pictureURL` varchar(255) DEFAULT NULL,
  `lastModified` datetime DEFAULT NULL,
  `shop_id` int(20) DEFAULT NULL,
  `currency_id` int(20) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `altitude` double DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `Shop` (
  `id` int(11) DEFAULT NULL,
  `user_id` int(20) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `isOnline` bit(1) DEFAULT NULL,
  `shopURL` varchar(255) DEFAULT NULL,
  `phoneNumber` varchar(128) DEFAULT NULL,
  `pictureURL` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `houseNumber` varchar(255) DEFAULT NULL,
  `country` varchar(128) DEFAULT NULL,
  `poBox` varchar(128) DEFAULT NULL,
  `availtimes_id` int(20) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `altitude` double DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `SubCategory` (
  `id` int(11) DEFAULT NULL,
  `subCategoryTranslationKey` varchar(128) DEFAULT NULL,
  `defaultPic` varchar(128) DEFAULT NULL,
  `category_id` int(20) DEFAULT NULL,
  `infoTranslationKey` varchar(128) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE `User` (
  `id` int(11) DEFAULT '0',
  `isRegistered` bit(1) DEFAULT b'0',
  `isValidated` bit(1) DEFAULT b'0',
  `firstLogin` datetime DEFAULT NULL,
  `loginNumber` int(11) DEFAULT NULL,
  `passwordHash` varchar(255) DEFAULT NULL,
  `searchRange` int(11) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `altitude` double DEFAULT NULL,
  `isRegistrationNotComplete` bit(1) DEFAULT b'1',
  `registrationKey` varchar(255) DEFAULT NULL,
  `loginName` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;




SET FOREIGN_KEY_CHECKS = 0;


LOCK TABLES `Alarm` WRITE;
INSERT INTO `Alarm` (`id`, `date`, `user_id`, `prize`, `category_id`, `subcategory_id`, `latitude`, `longitude`, `altitude`, `alarmOnline`, `numberOfItems`, `currency_id`) VALUES (1, '2010-08-02 00:23:51', 1, 0.5, 1, 1, 52.523405, 11.583, 0, b'0', 5, NULL);
UNLOCK TABLES;


LOCK TABLES `Availtime` WRITE;
INSERT INTO `Availtime` (`id`, `openingTime`, `closingTime`, `monday`, `tuesday`, `wednesday`, `thursday`, `friday`, `saturday`, `sunday`) VALUES (1, '10:00:00', '19:00:00', b'1', b'1', b'1', b'1', b'1', b'0', b'0');
UNLOCK TABLES;


LOCK TABLES `Category` WRITE;
INSERT INTO `Category` (`id`, `categoryTranslationKey`, `defaultPic`, `infoTranslationKey`) VALUES (1, 'categories.mask', 'https://www.maskradar.de/mask.jpg', 'categories.info.mask'), (2, 'categories.gloves', 'https://www.maskradar.de/gloves.jpg
', 'categories.info.gloves'), (3, 'categories.desinfection', 'https://www.maskradar.de/desinfection.jpg', 'categories.info.desinfection'), (4, 'categories.other', 'https://www.masradar.de/other.jpg', 'categories.info.other');
UNLOCK TABLES;


LOCK TABLES `Comment` WRITE;
INSERT INTO `Comment` (`id`, `commentText`, `date`, `item_id`, `user_id`) VALUES (1, 'Super Produkt', '2010-08-02 00:23:51', 1, 1);
UNLOCK TABLES;


LOCK TABLES `Currency` WRITE;
INSERT INTO `Currency` (`id`, `currency`) VALUES (1, 'EUR'), (2, 'RUBEL'), (3, 'YEN');
UNLOCK TABLES;


LOCK TABLES `Item` WRITE;
INSERT INTO `Item` (`id`, `name`, `description`, `creationDate`, `user_id`, `category_id`, `subcategory_id`, `numberAvail`, `isVerified`, `price`, `pricePerUnit`, `orderURL`, `pictureURL`, `lastModified`, `shop_id`, `currency_id`, `latitude`, `longitude`, `altitude`) VALUES (1, 'Weisse Masken - chemical

', 'Sehr schöne weisse Baumwollmasken, angenehm zu tragen', '2010-08-02 00:23:51', 1, 1, 1, 100, b'1', 10, 1, 'https://www.maskshop.de/weisse_maske.html', 'www.maskradar.de/ItemPics/weisse_masken_item.jpg
', '2010-08-02 00:23:51', 1, 1, 49.494034128078, 8.45711420315853, 0), (2, 'blaue Masken - biological
', 'fesche blaue Masken für Oktoberfest', '2011-09-02 00:23:51', 1, 1, 3, NULL, b'0', 5, 0.5, NULL, '2
', '2010-04-03 00:23:51', NULL, 1, 48.494034128078, 8.35711420315853, 0);
UNLOCK TABLES;


LOCK TABLES `Shop` WRITE;
INSERT INTO `Shop` (`id`, `user_id`, `name`, `isOnline`, `shopURL`, `phoneNumber`, `pictureURL`, `city`, `street`, `houseNumber`, `country`, `poBox`, `availtimes_id`, `latitude`, `longitude`, `altitude`) VALUES (1, 1, 'MaskShop', b'0', 'https://www.maskshop.de', '07231 299666', 'https://www.maskradar.de/shop.jpg
', 'Pforzheim
', 'St.Georgen Steige', '56', 'Deutschland', '75173', 1, 49.494034128078, 8.45711420315853, 0), (2, 1, 'Online Mask Shop', b'1', 'https://www.maskshop.de', '089 25004946', 'https://www.maskradar.de/shop-online.de', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
UNLOCK TABLES;


LOCK TABLES `SubCategory` WRITE;
INSERT INTO `SubCategory` (`id`, `subCategoryTranslationKey`, `defaultPic`, `category_id`, `infoTranslationKey`) VALUES (1, 'subCategories.mask.white', 'https://www.maskradar.de/weisse_maske.jpg', 1, 'subCategories.info.mask.white'), (2, 'subCategories.mask.red', 'https://www.maskradar.de/rote_maske.jpg', 1, 'subCategories.info.mask.red'), (3, 'subCategories.mask.blue', 'https://www.maskradar.de/blaue_maske.jpg', 1, 'subCategores.info.mask.blue'), (4, 'subCategories.gloves.white', 'https://www.maskradar.de/weisse_handschuhe.jpg', 2, 'subCategories.info.gloves.white'), (5, 'subCategories.gloves.red', 'https://www.maskradar.de/rote_handschuhe.jpg', 2, 'subCategories.info.gloves.red'), (6, 'subCategories.gloves.blue', 'https://www.maskradar.de/blaue_handschuhe.jpg', 2, 'subCategories.info.gloves.blue');
UNLOCK TABLES;


LOCK TABLES `User` WRITE;
INSERT INTO `User` (`id`, `isRegistered`, `isValidated`, `firstLogin`, `loginNumber`, `passwordHash`, `searchRange`, `latitude`, `longitude`, `altitude`, `isRegistrationNotComplete`, `registrationKey`, `loginName`) VALUES (1, b'1', b'1', '2010-08-02 00:23:51', 5, '41838537a23a5940471dfe7d58fd264a', 2500, 52.523405, 11.583, 0, b'0', 'OjRTBzmgYkauYsRMNGV2zfD7CbOaPC', 'Anton
');
UNLOCK TABLES;




SET FOREIGN_KEY_CHECKS = 1;


