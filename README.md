# MaskRadar

maskradar.py is an interface between user interfaces(e.g. mobile devices) and various data sources used by MaskRadar. It is implemented in a form of REST API, and can be used as shown below:

## View an item

      curl "http://35.198.183.20:443/viewItem?lang=german&user=1&itemID=1"
      [{"Altitude":0.0,"Category":"Masken","CreatorUser":"Anton\n","Currency":"EUR","ErrMsg":"","ItemDescription":"Sehr sch\u00f6ne weisse Baumwollmasken, angenehm zu tragen","ItemName":"Weisse Masken - chemical\n\n","LastModificationDate":"Mon, 02 Aug 2010 00:23:51 GMT","Latitude":49.494034128078,"Longitude":8.45711420315853,"NumberAvailable":100,"OrderURL":"https://www.maskshop.de/weisse_maske.html","PictureURL":"www.maskradar.de/ItemPics/weisse_masken_item.jpg\n","Price":10.0,"PricePerItem":1.0,"ResponseCode":"OK","ShopName":"MaskShop","SubCategory":"weisseMasken","UserVerified":"\u0001"}]

## List available categories with subcategoris

      curl "http://35.198.183.20:443/getCategories?lang=german"
      {"ErrTitle":"","SendCategoryTypes":{"Categories":[{"Category":"Masken","CategoryId":1,"SubCategories":[{"SubCategory":"weisseMasken","SubCategoryId":1},{"SubCategory":"roteMasken","SubCategoryId":2},{"SubCategory":"blaueMasken","SubCategoryId":3}]},{"Category":"Handschuhe","CategoryId":2,"SubCategories":[{"SubCategory":"weisseHandschuhe","SubCategoryId":4},{"SubCategory":"roteHandschuhe","SubCategoryId":5},{"SubCategory":"blaueHandschuhe","SubCategoryId":6}]},{"Category":"Desinfektionsmittel","CategoryId":3,"SubCategories":[]},{"Category":"Sonstiges","CategoryId":4,"SubCategories":[]}],"ErrMsg":"","ResponseCode":"OK"}}

## Get available information about a subcategory

      curl "http://35.198.183.20:443/getCategoryInfo?lang=german&categoryId=1&subCategoryId=1"
      {"sendCategoryInfo":{"SubCategoryInfo":"https://www.maskradar.de/info/german/subcategories_info_mask_white.html","errMsg":"","errTitle":"","responseCode":"OK"}}

## Comments about the code

* The code is written in Python 3.7
* The REST API is implemented using Flask and two modules are required: flask, flask_mysql. Look at _import_ statements for the exact list of dependencies
* It uses data stored in MySQL. The schema with some test data is in maskradar.sql
* The code supports translations of item properties. The translations for German are in "i18nTranslations_de.properties" and have the following formi(key=vaue):

     > categories.gloves = Handschuhe

     Keys are referenced by MySQL tables(e.g. columns subCategoryTranslationKey and infoTranslationKey in the table SubCategory)
* To start the service you have to execute

      python3.7 maskradar.py

     It will listen on the port 443 per default. You may need to open it for non-root users first:

      sudo setcap 'cap_net_bind_service=+ep'  `which python3.7`
