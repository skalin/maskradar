from flask import jsonify
from flask import flash, request
from flask import Flask
from flask_mysqldb import MySQL
import json
import datetime

app = Flask(__name__)

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'test'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'maskradar'
app.debug = False

mysql = MySQL(app)

def myconverter(o):
    if isinstance(o,datetime.datetime):
        return o.__str__()

@app.route('/user')
def user():
    message = {
      'status': 200,
      'message':'user '+request.url,
    }
    resp = jsonify(message)
    resp.status_code=200

    return resp

@app.route('/offer', methods=['GET','POST'])
def offer():
    try:
        if request.method == 'GET':
            cur = mysql.connection.cursor()
            cur.execute('SELECT * FROM offer LIMIT 10;')
            row_headers=[x[0] for x in cur.description]
            rows = cur.fetchall()
            #rows = rows.json_encoder()
            print(rows[0])
            json_data=[]
            for row in rows:
                json_data.append(dict(zip(row_headers, row)))
            #resp = jsonify(rows)
            print(json.dumps(json_data,default=myconverter))
            resp = jsonify(json.dumps(json_data,default=myconverter))
            resp.status_code=200
            return resp
    except Exception as e:
        print(e)

@app.route('/getShops', methods=['GET'])
def getShops():
    try:
        if request.method == 'GET':
            lang = request.args.get('lang', default='german',type=str)
            cur = mysql.connection.cursor()
            cur.execute('SELECT * FROM Shop;')
            row_headers=[x[0] for x in cur.description]
            rows = cur.fetchall()
            #rows = rows.json_encoder()
            print(rows[0])
            json_data=[]
            for row in rows:
                json_data.append(dict(zip(row_headers, row)))
            #resp = jsonify(rows)
            print(json.dumps(json_data,default=myconverter))
            resp = jsonify(json.dumps(json_data,default=myconverter))
            resp.status_code=200
            return resp
    except Exception as e:
        print(e)

@app.route('/getCategories', methods=['GET'])
def getCategories():
    try:
        if request.method == 'GET':
            lang = request.args.get('lang', default='german',type=str)
            cur = mysql.connection.cursor()
            cur.execute('SELECT * FROM SubCategory;')
            subCategories={}
            for(id,subCategoryTranslationKey,defaultPic,category_id,infoTranslationKey) in cur:
                if category_id not in subCategories.keys(): subCategories[category_id] = []
                subCategories[category_id].append({'SubCategoryId':id, 'SubCategory':translations[lang][subCategoryTranslationKey]})
            #print(subCategories)

            cur.execute('SELECT * FROM Category;')
            #rows = cur.fetchall()
            json_data=[]
            #for row in cur:
            for(id,categoryTranslationKey,defaultPic,infoTranslationKey) in cur:
                #print(id)
                if id not in subCategories.keys(): subCategories[id] = []
                json_data.append({'CategoryId':id, 'Category':translations[lang][categoryTranslationKey],'SubCategories':subCategories[id]})

            cur.close()

            # add neccessary fields
            json_data = {'SendCategoryTypes':{'ErrMsg':'','ResponseCode':'OK','Categories':json_data},'ErrTitle':''}
            #resp = jsonify(json.dumps(json_data,default=myconverter))
            resp = jsonify(json_data)
            resp.status_code=200
            return resp
    except Exception as e:
        print(e)

@app.route('/getCategoryInfo', methods=['GET'])
def getCategoryInfo():
    try:
        if request.method == 'GET':
            lang = request.args.get('lang', default='german',type=str)
            category_id = request.args.get('categoryId', default=1,type=int)
            subCategory_id = request.args.get('subCategoryId', default=1,type=int)
            cur = mysql.connection.cursor()
            cur.execute('SELECT infoTranslationKey FROM SubCategory WHERE id='+str(subCategory_id)+';')
            infoTranslationKey = cur.fetchone()[0]
            json_data = {"sendCategoryInfo":{"errMsg":"","responseCode":"OK","SubCategoryInfo":translations[lang][infoTranslationKey],"errTitle":""}}
            cur.close()
            resp = jsonify(json_data)
            resp.status_code=200
            return resp
    except Exception as e:
        print(e)

@app.route('/viewItem', methods=['GET'])
def viewItem():
    try:
        if request.method == 'GET':
            lang = request.args.get('lang', default='german',type=str)
            user = request.args.get('user', default=1,type=str)
            itemId = request.args.get('itemID', default=1,type=str)
            cur = mysql.connection.cursor()
            cur.execute('SELECT * FROM Item WHERE user_id='+user+' AND id='+itemId+';')
            items = {}
            for(id,name,description,creationDate,user_id,category_id,subcategory_id,numberAvail,isVerified,price,pricePerUnit,orderURL,pictureURL,lastModified,shop_id,currency_id,latitude,longitude,altitude) in cur:
                cur.execute('SELECT subCategoryTranslationKey FROM SubCategory WHERE id='+str(subcategory_id)+';')
                subCategoryTranslationKey = cur.fetchone()[0]
                cur.execute('SELECT categoryTranslationKey FROM Category WHERE id='+str(category_id)+';')
                categoryTranslationKey=cur.fetchone()[0]
                cur.execute('SELECT loginName FROM User WHERE id='+user+';')
                creatorUser=cur.fetchone()[0]
                cur.execute('SELECT name FROM Shop WHERE id='+str(shop_id)+';')
                shop_name=cur.fetchone()[0]
                cur.execute('SELECT currency FROM Currency WHERE id='+str(currency_id)+';')
                currency_id=cur.fetchone()[0]

                items = {'ErrMsg':'','ResponseCode':'OK','ItemName':name,'ItemDescription':description,'Category':translations[lang][categoryTranslationKey],'SubCategory':translations[lang][subCategoryTranslationKey],'CreatorUser':creatorUser,'UserVerified':isVerified,'LastModificationDate':lastModified,'ShopName':shop_name,'NumberAvailable':numberAvail,'Price':price,'PricePerItem':pricePerUnit,'Currency':currency_id,'OrderURL':orderURL,'PictureURL':pictureURL,'Latitude':latitude,'Longitude':longitude,'Altitude':altitude}
            json_data=[items]
            cur.close()
            resp = jsonify(json_data)
            resp.status_code=200
            return resp
    except Exception as e:
        print(e)

@app.errorhandler(404)
def not_found(error=None):
   message = {
     'status': 404,
     'message':'Not found: '+request.url,
   }
   resp = jsonify(message)
   resp.status_code=404

   return resp

translations = {'german':{}}

categories={}
subCategories={}

def read_translations():
    f = open('i18nTranslations_de.properties','r')
    for line in f.readlines():
        line=line.strip().replace(' ','').replace('\t','')
        if line=='': continue
        o = line.split('=')[0]
        t = line.split('=')[1]
        translations['german'][o]=t
    print(translations)

if __name__ == '__main__':
   read_translations()
   #app.run(debug=True)
   #app.run(ssl_context='adhoc', host='0.0.0.0', port=443)
   app.run(host='0.0.0.0', port=443)
